﻿using Azure;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PdfFileService.Abstracts;
using PdfFileService.Models;
using PdfFileService.Persistence;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PdfFileService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PdfServiceController : ControllerBase
    {
        private readonly ILogger<PdfServiceController> _logger;
        private readonly IAzureStorageHelper _azureStorageHelper;

        public PdfServiceController(ILogger<PdfServiceController> logger, IAzureStorageHelper azureStorageHelper)
        {
            _logger = logger;

            _azureStorageHelper = azureStorageHelper;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                return Content("File not selected.");
            }

            if (file.Length > (5 * 1024 * 1024))
            {
                return BadRequest("Maximum allowed file size is 5 MB.");
            }

            // TODO: Add check on valid ContentType for file
            //if (file.ContentType != 'PDF')
            //    return BadRequest();

            // TODO: Move to service
            await _azureStorageHelper.CreateContainerAsync("pdfdocuments");

            Response<BlobContentInfo> uploadResponse;
            await using (var stream = file.OpenReadStream())
            {
                uploadResponse = await _azureStorageHelper
                    .UploadAsync("pdfdocuments", file.FileName, stream);
            }

            var result = uploadResponse.GetRawResponse();

            if (result.Status == (int)HttpStatusCode.Created)
            {
                var document = new PdfDocument
                {
                    Created = DateTime.UtcNow,
                    Updated = DateTime.UtcNow,
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    Id = Guid.NewGuid(),
                    Rate = 0
                };

                await DocumentDBRepository<PdfDocument>.CreateItemAsync(document);

                _logger.LogInformation($"Uploaded and stored pdf file - {file.FileName}.");

                return this.Ok(result);
            }

            _logger.LogError($"File can't be uploaded: {result.Status} - {result.ReasonPhrase}.");

            return this.BadRequest();
        }

        [HttpDelete, Route("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var file = await DocumentDBRepository<PdfDocument>.GetItemAsync(id);
            if (file != null)
            {
                // TODO: Move to service
                await _azureStorageHelper.CreateContainerAsync("pdfdocuments");

                var result = await _azureStorageHelper.DeleteAsync("pdfdocuments", file.FileName);

                if (result.Value)
                {
                    await DocumentDBRepository<PdfDocument>.DeleteItemAsync(id);
                }
            }

            return this.Ok();
        }

        [HttpPost("/documentbase64")]
        public async Task<IActionResult> UploadForBase64(string base64)
        {
            // TODO: should convert from base64 to stream and process current object

            return this.Ok();
        }
    }
}