﻿using Microsoft.AspNetCore.Mvc;

namespace PdfFileService.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}