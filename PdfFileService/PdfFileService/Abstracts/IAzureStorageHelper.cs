﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System.IO;
using System.Threading.Tasks;

namespace PdfFileService.Abstracts
{
    public interface IAzureStorageHelper
    {
        Task CreateContainerAsync(string containerName);
        BlobClient GetBlobClient(string containerName, string blobName);
        Task<Response<BlobContentInfo>> UploadAsync(string containerName, string blobName, Stream content);
        Task<Response<bool>> DeleteAsync(string containerName, string blobName);
    }
}
