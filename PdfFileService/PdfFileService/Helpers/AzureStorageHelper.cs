﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PdfFileService.Abstracts;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PdfFileService.Helpers
{
    public class AzureStorageHelper : IAzureStorageHelper
    {
        private readonly ILogger<AzureStorageHelper> logger;
        private readonly IConfiguration config;
        public AzureStorageHelper(IConfiguration config, ILoggerFactory loggerFactory)
        {
            this.config = config;
            this.logger = loggerFactory.CreateLogger<AzureStorageHelper>();
        }

        public async Task CreateContainerAsync(string containerName)
        {
            try
            {
                var container = new BlobContainerClient(this.config.GetConnectionString("StorageConnectionString"), containerName);

                await container.CreateIfNotExistsAsync();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, ex.Message);
            }
        }

        public BlobClient GetBlobClient(string containerName, string blobName)
        {
            try
            {
                var container = new BlobContainerClient(this.config.GetConnectionString("StorageConnectionString"), containerName);
                var blob = container.GetBlobClient(blobName);

                return blob;
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, ex.Message);
            }

            return null;
        }

        public async Task<Response<BlobContentInfo>> UploadAsync(string containerName, string blobName, Stream content)
        {
            try
            {
                var blob = this.GetBlobClient(containerName, blobName);

                 return await blob.UploadAsync(content);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, ex.Message);
            }

            return null;
        }

        public async Task<Response<bool>> DeleteAsync(string containerName, string blobName)
        {
            try
            {
                var blob = this.GetBlobClient(containerName, blobName);

                return await blob.DeleteIfExistsAsync();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, ex.Message);
            }

            return null;
        }
    }
}
